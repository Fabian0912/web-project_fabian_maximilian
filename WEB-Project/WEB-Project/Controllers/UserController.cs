﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using WEBProject.Models;
using WEBProject.Models.db;

namespace WEBProject.Controllers
{
    public class UserController : Controller
    {
        private IUserRepository usersRepository;


        public ActionResult AdminUsers()
        {

            if ((Session["isAdmin"] != null) && (Convert.ToInt32(Session["isAdmin"]) == 1))
            {

                try
                {
                    usersRepository = new UserRepositoryDB();
                    usersRepository.Open();
                    List<User> us = usersRepository.GetAllUsers();
                    usersRepository.Close();

                    return View(us);
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "", "Probleme mit der Datenbank.", "Versuchen Sie es später erneut."));
                }
                finally
                {
                    usersRepository.Close();
                }
            }
            return View("Message", new Message("Fehler", "", "Die angegebene URL ist nicht gültig.", ""));
        }
    
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UserProfile()
        {
            User u = (User)(Session["User"]);
            if (u == null)
            {
                return View("Message", new Message("Fehler", "", "Kein Eintrag gefunden", ""));
            }
            else
            {
                return View(u);
            }
        }
        [HttpGet]
        public ActionResult Register()
        {
            User u = new User();
            return View(u);
        }
        public ActionResult Register(User userDataFromForm)
        {
            if (userDataFromForm != null)
            {
                ValidateRegistrationForm(userDataFromForm, true);


                if (ModelState.IsValid)
                {
                    try
                    {
                        usersRepository = new UserRepositoryDB();
                        usersRepository.Open();
                        usersRepository.Insert(userDataFromForm);
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "", "Datenbankfehler", ""));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Fehler", "", "Fehler bei der Registrierung", ""));
                    } //

                    finally
                    {
                        usersRepository.Close();
                    }


                    return View("Message", new Message("Registrierung", "", "Sie wurden erfolgreich auf unserer Homepage registriert!", ""));
                }
                else
                {

                    return View(userDataFromForm);
                }
            }


            return View();
        }
        public void ValidateRegistrationForm(User userDataFromForm, bool password)
        {

            if (userDataFromForm.Firstname == null)
            {
                ModelState.AddModelError("Firstname", "Sie müssen einen Vornamen eingeben");
            }
            if (userDataFromForm.Lastname == null)
            {
                ModelState.AddModelError("Lastname", "Sie müssen einen Nachanamen eingeben!");
            }
            if ((userDataFromForm.Birthdate >= DateTime.Now))
            {
                ModelState.AddModelError("Birthdate", "Dieses Datum ist ungültig!");
            }
            if (password)
            {
                if ((userDataFromForm.OldPassword == null) || (userDataFromForm.OldPassword.Trim().Length < 8) || (userDataFromForm.OldPassword.IndexOfAny(new char[] { '!', '?', '%', '&', '#' }) == -1))
                {
                    ModelState.AddModelError("Password", "Das Passwort muss mindestens 8 Zeichen enthalten und mindestens eines von diesen(!,?,#,%,&) Sonderzeichen enthalten");
                }
            }


        }
        [HttpGet]
        public ActionResult Login()
        {
            if ((Session["isAdmin"] == null) && (Session["isRegisteredUser"] == null))
            {
                Login u = new Login();
                return View(u);
            }
            else
            {
                return RedirectToAction("UserProfile");
            }

        }
        [HttpPost]
        public ActionResult Login(Login user)
        {

            if (user == null)
            {
                return View(user);
            }
            try
            {
                usersRepository = new UserRepositoryDB();
                usersRepository.Open();

                User loggedInUser = usersRepository.Authenticate(user.UsernameOrEMail, user.Password);
                if (loggedInUser.IsAdmin == true)
                {
                    Session["isAdmin"] = true;
                    Session["User"] = loggedInUser;
                    return RedirectToAction("index", "home");
                }
                else if (loggedInUser.IsAdmin == false)
                {
                    Session["isRegisteredUser"] = true;
                    Session["User"] = loggedInUser;
                    return RedirectToAction("index", "home");
                }
                else
                {
                    ModelState.AddModelError("UsernameOrEMail", "Benutzername/Email oder Passwort stimmen nicht!");
                    return View(user);
                }
            }
            catch (Exception)
            {
                return View("Message", new Message("Datenbankfehler", "", "Probleme mit der Datenbank!", ""));
            }
            finally
            {
                usersRepository.Close();
            }
        }
        public ActionResult Logout()
        {
            Session["isAdmin"] = null;
            Session["User"] = null;
            Session["isRegisteredUser"] = null;
            return RedirectToAction("index", "home");
        }
        [HttpGet]
        public ActionResult ChangeUserData()
        {

            try
            {
                User user = (User)Session["User"];
                return View(user);

            }
            catch (Exception)
            {
                return View("Message", new Message("Fehler", "", "Sie müssen sich anmelden!", ""));
            }
        }
        [HttpPost]
        public ActionResult ChangeUserData(User userData)
        {
            User us = (User)Session["User"];
            int id = us.UserID;
            if (userData != null)
            {

                //Formularüberprüfung
                ValidateRegistrationForm(userData, false);
                 
                if (ModelState.IsValid)
                {
                    try
                    {
                        usersRepository = new UserRepositoryDB();
                        usersRepository.Open();
                        if (usersRepository.ChangeUserData(id, userData))
                        {
                            return View("Message", new Message("Daten ändern", "", "Daten wurden erfolgreich geändert!", ""));
                        }
                        {
                            return View("Message", new Message("Fehler", "", "Ein Fehler ist augetreten", ""));
                        }
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "", "Datenbankfehler", ""));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Fehler", "", "andere Fehler", ""));
                    } 
                    finally
                    {
                        usersRepository.Close();
                    }
                }
                else
                {
                    return View(userData);
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                usersRepository = new UserRepositoryDB();
                usersRepository.Open();

                if (usersRepository.Delete(id))
                {
                    return RedirectToAction("AdminUsers");
                }
                else
                {
                    return View("Message", new Message("Benutzer löschen", "", "Der Benutzer konnte nicht gelöscht werden.", ""));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "", "Datenbankfehler", ""));
            }
            finally
            {
                usersRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(User user)
        {
            user = (User)Session["User"];
            int id = user.UserID;

            if (user != null) { 
                    try
                    {
                        usersRepository = new UserRepositoryDB();
                        usersRepository.Open();
                    if(usersRepository.ChangePassword(id, user)) {
                        user.OldPassword = user.NewPassword;
                        user.NewPassword = "";
                        RedirectToAction("Index");
                    }
                    else
                    {
                        return View("Message", new Message("Passwort falsch!", "", "Passwort falsch!", ""));
                    }

                }
                catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "", "Datenbankfehler", ""));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Fehler", "", "andere Fehler", ""));
                    } //
                    finally
                    {
                        usersRepository.Close();
                    }

                    return View("Message", new Message("Daten ändern", "", "Daten wurden erfolgreich geändert!", ""));
                }
                else
                {
                    return View(user);
                }
        }

    }

}



