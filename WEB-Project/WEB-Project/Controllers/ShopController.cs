﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using WEBProject.Models;
using WEBProject.Models.db;

namespace WEBProject.Controllers
{
    public class ShopController : Controller
    {
       
        private IArticleRepository articleRepository;

        public ActionResult Index()
        {
            return View ();
        }
        public ActionResult Shop()
        {
            return View();
        }
        public ActionResult DetailShop(int id)
        {

            try
            {
                articleRepository = new ArticleRepositoryDB();
                articleRepository.Open();
                Article a = articleRepository.GetArticle(id);
                return View(articleRepository.GetArticle(id));
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "", "Datenbankfehler", ""));
            }
            catch (Exception)
            {
                return View("Message", new Message("Fehler", "", "Fehler bei der Erstellung des Artikels", ""));
            } //
      
            finally
            {
                articleRepository.Close();
            }

        }





        public ActionResult Warenkorb(int id)
        {
            if ((Session["isAdmin"] != null ) || (Session["isRegisteredUser"] != null))
            {
                try
                {
                    articleRepository = new ArticleRepositoryDB();
                    articleRepository.Open();
                    List<Article> warenkorbArticles = new List<Article>();
                    warenkorbArticles.Add(articleRepository.GetArticle(id));
                    return View(warenkorbArticles);
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "", "Datenbankfehler", ""));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Fehler", "", "Fehler bei der Erstellung des Artikels", ""));
                }
                finally
                { articleRepository.Close(); }

            }
            else
            {
                return View("Login");
            }
        }
        public ActionResult ShowAllArticle()
        {
            List<Article> articles = LoadArticles();

            return View(articles);
        }
        private List<Article> LoadArticles()
        {
            if (ModelState.IsValid)
            {
                    articleRepository = new ArticleRepositoryDB();
                    articleRepository.Open();
                    List<Article> art =  articleRepository.GetAllArticles();
                    articleRepository.Close();
                
                return art;
            }
            else { return null; }
        }

        [HttpGet]
        public ActionResult InsertArticle() 
        {
            Article article = new Article();
            return View();
        }
        [HttpPost]
        public ActionResult InsertArticle(Article articleToInsert)
        {
            if (articleToInsert != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        articleRepository = new ArticleRepositoryDB();
                        articleRepository.Open();
                        articleRepository.InsertArticle(articleToInsert);
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "", "Datenbankfehler", ""));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Fehler", "", "Fehler bei der Erstellung der Artikels", ""));
                    } //
                   
                    finally
                    {
                        articleRepository.Close();
                    }


                    return View("Message", new Message("Artikel erstellung", "", "Artikel wurde erfolgreich erstellt!", ""));
                }
                else
                {
                    return View(articleToInsert);
                }
            }


            return View();
        }
    }
}
