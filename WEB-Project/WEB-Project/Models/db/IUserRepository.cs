﻿using System;
using System.Collections.Generic;

namespace WEBProject.Models.db
{
    public enum UserRole
    {
        Admin, RegisteredUser, NoUser
    }
    public interface IUserRepository
    {
        void Open();
        void Close();

        User GetUser(int userId);
        List<User> GetAllUsers();
        bool Insert(User userToInsert);
        bool Delete(int idToDelete);
        bool ChangeUserData(int userIdToChange, User newUserData);
        User Authenticate(string EmailAddress, string password);
        bool ChangePassword(int? id, User user);

    }
}
