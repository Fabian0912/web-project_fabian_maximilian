﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using MySql.Data.MySqlClient;
using System.Data;

namespace Einführungsbeispiel_Web.Models.db {
    public class UserRepositoryDB : IUsersRepository
{

    private string _connectionString = "Server=localhost;Database=web_5a;Uid=root;Pwd=12345";
    private MySqlConnection _connection;

    public void Open()
    {
       
    public void Close()
    {

        try
        {
            if (this._connection != null && this._connection.State == ConnectionState.Open)
            {
                this._connection.Close();
                this._connection = null;
            }
        }
        catch (MySqlException)
        {
            throw;
        }

    }

    // Authenticate gibt UserRole zur¸ck => hiermit kann sp‰ter ¸berpr¸ft werden
    //      wer sich eingeloggt hat => admin soll z.B. andere Rechte erhalten
    //      es ist auch mˆglich, dass Authenticate eine komplette Person inkl. UserRole zur¸ckgibt
    public User Authenticate(string usernameOrEMail, string password)
    {

        try
        {
            MySqlCommand cmdAuthenticate = this._connection.CreateCommand();
            cmdAuthenticate.CommandText = "SELECT user FROM users WHERE ((username = @usernameOrEMail) AND (passwrd = sha1(@password)) OR ((email = @usernameOrEMail) AND (passwrd = sha1(@password))))";
            cmdAuthenticate.Parameters.AddWithValue("usernameOrEMail", usernameOrEMail);
            cmdAuthenticate.Parameters.AddWithValue("password", password);

            using (MySqlDataReader reader = cmdAuthenticate.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    if (Convert.ToInt32(reader["isAdmin"]) == 1)
                    {
                        return UserRole.Admin;
                    }
                    else
                    {
                        return UserRole.RegisteredUser;
                    }
                }
                else
                {
                    return UserRole.NoUser;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// TODO: testen
    public bool ChangeUserData(int userIdToChange, User newUserData)
    {
        if (this._connection == null || this._connection.State != ConnectionState.Open)
        {
            return false;
        }

        try
        {
            MySqlCommand cmdChangeUserData = this._connection.CreateCommand();
            cmdChangeUserData.CommandText = "UPDATE users SET firstname=@firstname, lastname=@lastname, email=@email, username=@username, birthdate=@birthdate, passwrd=sha1(@password) WHERE id=@id";
            cmdChangeUserData.Parameters.AddWithValue("firstname", newUserData.Firstname);
            cmdChangeUserData.Parameters.AddWithValue("lastname", newUserData.Lastname);
            cmdChangeUserData.Parameters.AddWithValue("email", newUserData.EMail);
            cmdChangeUserData.Parameters.AddWithValue("username", newUserData.Username);
            cmdChangeUserData.Parameters.AddWithValue("birthdate", newUserData.Birthdate);
            cmdChangeUserData.Parameters.AddWithValue("password", newUserData.Password);
            cmdChangeUserData.Parameters.AddWithValue("id", userIdToChange);

            return cmdChangeUserData.ExecuteNonQuery() == 1;
        }
        catch (MySqlException)
        {
            throw;
        }
    }


    public bool Delete(int userIDToDelete)
    {
        if (this._connection == null || this._connection.State != ConnectionState.Open)
        {
            return false;
        }

        try
        {
            MySqlCommand cmdDelete = this._connection.CreateCommand();
            // alle Benutzer auﬂer dem Administrator d¸rfen gelˆscht werden
            cmdDelete.CommandText = "DELETE FROM users WHERE id = @idToDelete AND isAdmin = 0";
            cmdDelete.Parameters.AddWithValue("idToDelete", userIDToDelete);

            return cmdDelete.ExecuteNonQuery() == 1;
        }
        catch (MySqlException)
        {
            throw;
        }
    }

    public List<User> GetAllUsers()
    {

        if (this._connection == null || this._connection.State != ConnectionState.Open)
        {
            return null;
        }

        List<User> allUsers = new List<User>();
        try
        {
            MySqlCommand cmdSelectAllUsers = this._connection.CreateCommand();
            cmdSelectAllUsers.CommandText = "SELECT * FROM users";

            // ExecuteReader() wird bei Select-Abfragen verwendet
            using (MySqlDataReader reader = cmdSelectAllUsers.ExecuteReader())
            {
                // Read() liefert true/false zur¸ck
                //  true ... weitere Datens‰tze sind vorhanden
                //  false ... kein Datensatz mehr vorhanden
                while (reader.Read())
                {
                    // 0 bedeutet, dass es sich um einen normale User handelt
                    // 1 bedeutet, dass es sich um den Administrator handelt
                    if (Convert.ToInt32(reader["isAdmin"]) == 0)
                    {
                        allUsers.Add(
                        new User
                        {
                                // ID ... Feldname in der Klasse User
                                // "id" ... Name des Feldes in der DB-Tabelle
                                ID = Convert.ToInt32(reader["id"]),
                            Firstname = Convert.ToString(reader["firstname"]),
                            Lastname = Convert.ToString(reader["lastname"]),
                            EMail = Convert.ToString(reader["email"]),
                            Username = Convert.ToString(reader["username"]),
                            Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null
                        }
                        );
                    }
                }
            }
        }
        catch (MySqlException)
        {
            throw;
        }

        return allUsers.Count >= 1 ? allUsers : null;
    }

    /// TODO: testen
    public User GetUser(int userId)
    {
        if (this._connection == null || this._connection.State != ConnectionState.Open)
        {
            return null;
        }

        try
        {
            MySqlCommand cmdSelectUserById = this._connection.CreateCommand();
            cmdSelectUserById.CommandText = "SELECT * FROM users WHERE id = @userID";
            cmdSelectUserById.Parameters.AddWithValue("userID", userId);

            // ExecuteReader() wird bei Select-Abfragen verwendet
            using (MySqlDataReader reader = cmdSelectUserById.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    return new User()
                    {
                        ID = Convert.ToInt32(reader["id"]),
                        Firstname = Convert.ToString(reader["firstname"]),
                        Lastname = Convert.ToString(reader["lastname"]),
                        EMail = Convert.ToString(reader["email"]),
                        Username = Convert.ToString(reader["username"]),
                        Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null
                    };
                }
            }
        }
        catch (MySqlException)
        {
            throw;
        }

        return null;
    }

    public bool Insert(User userToInsert)
    {

        if (this._connection == null || this._connection.State != ConnectionState.Open)
        {
            return false;
        }

        try
        {
            MySqlCommand cmdInsert = this._connection.CreateCommand();
            // alle Eingabe m¸ssen mit Hilfe von Parametern im Command
            //      abgelegt werden => Problem: SqlInjection
            cmdInsert.CommandText = "INSERT INTO users VALUES(null, @firstname, @lastname, @email, @username, @birthdate, sha1(@pwd), 0)";
            cmdInsert.Parameters.AddWithValue("firstname", userToInsert.Firstname);
            cmdInsert.Parameters.AddWithValue("lastname", userToInsert.Lastname);
            cmdInsert.Parameters.AddWithValue("email", userToInsert.EMail);
            cmdInsert.Parameters.AddWithValue("username", userToInsert.Username);
            cmdInsert.Parameters.AddWithValue("birthdate", userToInsert.Birthdate);
            cmdInsert.Parameters.AddWithValue("pwd", userToInsert.Password);

            // ExecuteNonQuery() ... f¸r alles auﬂer SELECT
            //  diese Methode git die Anzahl der ƒnderungen zur¸ck
            return cmdInsert.ExecuteNonQuery() == 1;
        }
        catch (MySqlException)
        {
            throw;
        }
    }

}
}