﻿using System;
using System.Collections.Generic;

namespace WEBProject.Models.db
{
    public interface IArticleRepository
    {
        void Open();
        void Close();

        Article GetArticle(int articleID);
        List<Article> GetAllArticles();
        bool InsertArticle(Article articleToInsert);
        bool DeleteArticle(int articleIdToDelete);
        bool ChangeArticle(int articleIdToChange, User newArticleData);

    }
}
