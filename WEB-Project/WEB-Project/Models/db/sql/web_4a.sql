
use web_4a;
drop table users;
select * from users;
create table users(
id int not null auto_increment,
firstname varchar(100)null,
lastname varchar(100)not null,
birthdate date null,
gender tinyint null,
username varchar(100) null,
isAdmin boolean not null default false,
email varchar(100) not null unique,
passwrd varchar(40)not null,

constraint id_PK primary key(id)


)engine=InnoDB;

insert into users values(null,"Maximilian","Winkler","2000-12-18",null,"maxi", true,"maxi@gmail.com",sha1("Maxi123!"));
insert into users values(null,"Fabian","Volgger","2000-12-09",null,"fabi", true,"fabi@gmail.com",sha1("Fabi!!"));
insert into users values(null,"Alexander","Montag","2000-12-07",null,"alex", true,"alex@gmail.com",sha1("Alex!!"));


create table article(
articleID int not null auto_increment,
articleName varchar(100) not null,
price decimal not null,
brand varchar(100) not null, 
releaseDate date null,
category tinyint null,

constraint articleID_PK primary key(articleID)

)engine=InnoDB;


insert into article values(10,"MacBookAir",1100.00,"Apple","2010-12-07",null);
insert into article values(11,"MacBookPro",1600.00,"Apple","2008-10-06",1);
insert into article values(null,"iPhone X",1000.00,"Apple","2018-10-06",1);


select * from article;
select * from users;
drop table article;

update users set username = "fabian" where id = 2;

