﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace WEBProject.Models.db
{
    public class UserRepositoryDB : IUserRepository
    {
        private string _connectionString = "Server=localhost;Database=web_4a;Uid=root;PWD=12345678";
        private MySqlConnection _connection;

        public void Open()
        {
            if (this._connection == null)
            {
                this._connection = new MySqlConnection(this._connectionString);
            }

            if (this._connection.State != ConnectionState.Open)
            {
                this._connection.Open();
            }
        }

        public void Close()
        {
            if ((this._connection != null) || (this._connection.State != ConnectionState.Open))
            {
                this._connection.Close();
            }
        }

        public User Authenticate(string usernameOrEMail, string password)
        {

            try
            {
                MySqlCommand cmdAuthenticate = this._connection.CreateCommand();
                cmdAuthenticate.CommandText = "SELECT * FROM users WHERE ((username = @usernameOrEMail) AND (passwrd = sha1(@password)) OR ((email = @usernameOrEMail) AND (passwrd = sha1(@password))))";
                cmdAuthenticate.Parameters.AddWithValue("usernameOrEMail", usernameOrEMail);
                cmdAuthenticate.Parameters.AddWithValue("password", password);

                using (MySqlDataReader reader = cmdAuthenticate.ExecuteReader())
                {
                
                User user = new User();
                   if (reader.HasRows)
                    {
                        reader.Read();
                            user.UserID = Convert.ToInt32(reader["id"]);
                            user.Firstname = Convert.ToString(reader["firstname"]);
                            user.Lastname = Convert.ToString(reader["lastname"]);
                            user.Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null;
                            user.Gender = reader["gender"] != DBNull.Value ? (Gender)Convert.ToInt32(reader["gender"]) : Gender.notSpecified;
                            user.Username = Convert.ToString(reader["username"]);
                            user.EmailAddress = Convert.ToString(reader["email"]);
                            
                        if (Convert.ToInt32(reader["isAdmin"]) == 1)
                        {
                            user.IsAdmin = true;
                        }
                        else
                        {
                            user.IsAdmin = false;
                        }
                        return user;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ChangeUserData(int userIdToChange, User newUserData)
        {
            try
            {

                MySqlCommand cmdChangeData = this._connection.CreateCommand();
                cmdChangeData.CommandText = "UPDATE users SET firstname = @firstname,lastname = @lastname,birthdate = @birthdate,gender = @gender,username = @username,email = @email WHERE id = @id";
                cmdChangeData.Parameters.AddWithValue("firstname", newUserData.Firstname);
                cmdChangeData.Parameters.AddWithValue("lastname", newUserData.Lastname);
                cmdChangeData.Parameters.AddWithValue("birthdate", newUserData.Birthdate);
                cmdChangeData.Parameters.AddWithValue("gender", newUserData.Gender);
                cmdChangeData.Parameters.AddWithValue("username", newUserData.Username);
                cmdChangeData.Parameters.AddWithValue("email", newUserData.EmailAddress);
                cmdChangeData.Parameters.AddWithValue("id", userIdToChange);

                return cmdChangeData.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                throw;
            }
        }

        public bool Delete(int idToDelete)
        {
            if (idToDelete <= 0)
            {
                return false;
            }
            try
            {
                MySqlCommand cmdDelete = this._connection.CreateCommand();
                // Parameter verwenden(z.B. @firstname um SQL-Injections zu verhindern)
                cmdDelete.CommandText = "DELETE FROM users WHERE id = @id";
                cmdDelete.Parameters.AddWithValue("id", idToDelete);
                //ExecuteNonQuery() ... alle sql-befehle außer select werden mit diesem befehl abgesendet 
                //diese Methode gibt die Anzahl der betroffenen Datensätze in der Datenbank zurück
                return cmdDelete.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<User> GetAllUsers()
        {
            List<User> allUsers = new List<User>();

            try
            {
                MySqlCommand cmdAllUsers = this._connection.CreateCommand();
                cmdAllUsers.CommandText = "select * from users";

                using (MySqlDataReader reader = cmdAllUsers.ExecuteReader())
                {
                    while (reader.Read())
                    { 
                        allUsers.Add(
                            new User
                            {
                                UserID = Convert.ToInt32(reader["id"]),
                                Firstname = Convert.ToString(reader["firstname"]),
                                Lastname = Convert.ToString(reader["lastname"]),
                                Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                                Gender = reader["gender"] != DBNull.Value ? (Gender)Convert.ToInt32(reader["gender"]) : Gender.notSpecified,
                                Username = Convert.ToString(reader["username"]),
                                EmailAddress = Convert.ToString(reader["email"]),
                            });
                    }
                }
                return allUsers.Count > 0 ? allUsers : null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public User GetUser(int userId)
        {
            User user;
            try
            {
                MySqlCommand cmdUser = this._connection.CreateCommand();
                cmdUser.CommandText = "select * from users where id = @id";
                cmdUser.Parameters.AddWithValue("id", userId);

                using (MySqlDataReader reader = cmdUser.ExecuteReader())
                {
                    reader.Read();
                    user = new User
                    {
                        UserID = Convert.ToInt32(reader["id"]),
                        Firstname = Convert.ToString(reader["firstname"]),
                        Lastname = Convert.ToString(reader["lastname"]),
                        Birthdate = reader["birthdate"] != DBNull.Value ? Convert.ToDateTime(reader["birthdate"]) : (DateTime?)null,
                        Gender = reader["gender"] != DBNull.Value ? (Gender)Convert.ToInt32(reader["gender"]) : Gender.notSpecified,
                        Username = Convert.ToString(reader["username"]),
                        EmailAddress = Convert.ToString(reader["email"]),
                    };
                }

                return user != null ? user : null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Insert(User userToInsert)
        {
            if (userToInsert == null)
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsert = this._connection.CreateCommand();
                cmdInsert.CommandText = "INSERT INTO users VALUES(null,@firstname,@lastname, @birthdate, @gender, @username,false,@email,sha1(@pwd))";
               
                cmdInsert.Parameters.AddWithValue("firstname", userToInsert.Firstname);
                cmdInsert.Parameters.AddWithValue("lastname", userToInsert.Lastname);
                cmdInsert.Parameters.AddWithValue("birthdate", userToInsert.Birthdate);
                cmdInsert.Parameters.AddWithValue("gender", userToInsert.Gender);
                cmdInsert.Parameters.AddWithValue("username", userToInsert.Username);
                cmdInsert.Parameters.AddWithValue("email", userToInsert.EmailAddress);
                cmdInsert.Parameters.AddWithValue("pwd", userToInsert.OldPassword);
               
                return cmdInsert.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ChangePassword(int? id, User user)
        {
            if (id == null || user == null)
            {
                return false;
            }
            try
            {
                MySqlCommand cmdChangePwd = this._connection.CreateCommand();
                cmdChangePwd.CommandText = "UPDATE users SET passwrd = sha1(@newpassword) WHERE (passwrd = sha1(@oldpassword) AND (id = @id))";
                cmdChangePwd.Parameters.AddWithValue("@newpassword", user.NewPassword);
                cmdChangePwd.Parameters.AddWithValue("@oldpassword", user.OldPassword);
                cmdChangePwd.Parameters.AddWithValue("@id", id);


                return cmdChangePwd.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
        } 
    }
}
