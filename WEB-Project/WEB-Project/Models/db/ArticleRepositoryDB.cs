﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace WEBProject.Models.db
{
    public class ArticleRepositoryDB : IArticleRepository
    {
        private string _connectionString = "Server=localhost;Database=web_4a;Uid=root;PWD=12345678";
        private MySqlConnection _connection;

        public void Open()
        {
            if (this._connection == null)
            {
                this._connection = new MySqlConnection(this._connectionString);
            }

            if (this._connection.State != ConnectionState.Open)
            {
                this._connection.Open();
            }
        }

        public void Close()
        {
            if ((this._connection != null) || (this._connection.State != ConnectionState.Open))
            {
                this._connection.Close();
            }
        }




        public bool ChangeArticle(int articleIdToChange, User newArticleData)
        {
            throw new NotImplementedException();
        }
        public bool DeleteArticle(int articleIdToDelete)
        {
            throw new NotImplementedException();
        }

        public List<Article> GetAllArticles()
        {
            {
                try
                {
                    List<Article> allArticles = new List<Article>();
                    MySqlCommand cmdAllArticles = this._connection.CreateCommand();
                    cmdAllArticles.CommandText = "SELECT * FROM article";

                    using (MySqlDataReader reader = cmdAllArticles.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            allArticles.Add(new Article
                            {
                                ArticleID = Convert.ToInt32(reader["articleID"]),
                                ArticleName = Convert.ToString(reader["articleName"]),
                                Price = Convert.ToDecimal(reader["price"]),
                                Category = (reader["category"]) != DBNull.Value ? (Category)Convert.ToInt32(reader["category"]) :(Category?)null,
                                Brand = Convert.ToString(reader["brand"]),
                                ReleaseDate = reader["releaseDate"] != DBNull.Value ? Convert.ToDateTime(reader["releaseDate"]) : (DateTime?)null

                              
                            });

                        }

                    }

                    if (allArticles.Count > 0)
                    {
                        return allArticles;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception) { throw; }
            }
        }

        public Article GetArticle(int articleID)
        {
            Article article;
            try
            {
                MySqlCommand cmdUser = this._connection.CreateCommand();
                cmdUser.CommandText = "select * from article where articleID = @articleID";
                cmdUser.Parameters.AddWithValue("articleID", articleID);

                using (MySqlDataReader reader = cmdUser.ExecuteReader())
                {
                    reader.Read();
                    article = new Article
                    {
                        ArticleID = Convert.ToInt32(reader["articleID"]),
                        ArticleName = Convert.ToString(reader["articleName"]),
                        Price = Convert.ToDecimal(reader["price"]),
                        Category = (reader["category"]) != DBNull.Value ? (Category)Convert.ToInt32(reader["category"]) : (Category?)null,
                        Brand = Convert.ToString(reader["brand"]),
                        ReleaseDate = reader["releaseDate"] != DBNull.Value ? Convert.ToDateTime(reader["releaseDate"]) : (DateTime?)null,
                    };
                }

                return article != null ? article : null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool InsertArticle(Article articleToInsert)
        {
            if (articleToInsert == null)
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsert = this._connection.CreateCommand();
               
                cmdInsert.CommandText = "INSERT INTO article VALUES(null,@articleName,@price, @brand, @category, @releaseDate)";
                cmdInsert.Parameters.AddWithValue("articleName", articleToInsert.ArticleName);
                cmdInsert.Parameters.AddWithValue("price", articleToInsert.Price);
                cmdInsert.Parameters.AddWithValue("brand", articleToInsert.Brand);
                cmdInsert.Parameters.AddWithValue("category", articleToInsert.Category);
                cmdInsert.Parameters.AddWithValue("releaseDate", articleToInsert.ReleaseDate);
                return cmdInsert.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
