﻿using System;
namespace WEBProject.Models
{
    public enum Gender
    {
        male, female, notSpecified
    }
    public class User
    {
        public int UserID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime? Birthdate { get; set; }
        public Gender Gender { get; set; }
        public string Username { get; set; }
        public bool IsAdmin { get; set; }
        public string EmailAddress { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
       
        public User() : this(0, "", "", DateTime.Now, Gender.notSpecified, "",false,"", "", "") { }
        public User(int userID, string firstname, string lastname, DateTime? Birthdate, Gender gender, string username,bool isAdmin, string emailAddress, string oldPassword, string newPassword)
        {
            this.UserID = userID;
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Birthdate = Birthdate;
            this.Gender = gender;
            this.Username = username;
            this.IsAdmin = isAdmin;
            this.EmailAddress = emailAddress;
            this.NewPassword = newPassword;
            this.OldPassword = oldPassword;


        }

        public static implicit operator User(Login v)
        {
            throw new NotImplementedException();
        }
    }
}
