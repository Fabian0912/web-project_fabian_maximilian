﻿using System;
namespace WEBProject.Models
{
    public class Message
    {
        //Diese KLasse wird immer dann verwendet wenn wir irgendeoine erfolgs bzw fehlermeldung ausgeben möchten
        public string Header { get; set; }
        public string AdditionalHeader { get; set; }
        public string MessageText { get; set; }
        public string Solution { get; set; }

        public Message() : this("", "", "", "") { }
        public Message(string header, string additionalheader, string messageText, string solution)
        {
            this.Header = header;
            this.AdditionalHeader = additionalheader;
            this.MessageText = messageText;
            this.Solution = solution;

        
    
        }
    }
}
