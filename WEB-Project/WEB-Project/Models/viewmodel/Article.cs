﻿using System;
namespace WEBProject.Models
{
   public enum Category { 
        clothing, electronicalDevices, notSpecified, watches, cars
   }

    public class Article
    {
        public int ArticleID { get; set; }
        public string ArticleName { get; set; }
        public decimal Price { get; set; }
        public string Brand { get; set; }
        public Category? Category { get; set; }
        public DateTime? ReleaseDate { get; set; }

        public Article() : this(0,"", 0.0m, "",null, DateTime.Now ) { }
        public Article(int articleID, string articleName, decimal price,  string brand, Category? category, DateTime? releaseDate) {
            this.ArticleID = articleID;
            this.ArticleName = articleName;
            this.Price = price;
            this.Brand = brand;
            this.Category = category;
            this.ReleaseDate = releaseDate;
        }
    }
}
