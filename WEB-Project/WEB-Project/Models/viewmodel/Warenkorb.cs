﻿using System;
using System.Collections.Generic;

namespace WEBProject.Models.viewmodel
{
    public class Warenkorb
    {

        public List<Article> WarenkorbKlasse { get; set; }

        public Warenkorb() : this(null) { }
        public Warenkorb(List<Article> warenkorb) {
            this.WarenkorbKlasse = warenkorb;
        }
    }
}
